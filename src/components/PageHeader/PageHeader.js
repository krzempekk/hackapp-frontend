import React from 'react';

import "./PageHeader.scss";

const PageHeader = (props) => (
    <header className="PageHeader">
        <div>
            <div className="PageHeader-text">
                {props.text}
            </div>
            {
                props.subtext &&
                <div className="PageHeader-subtext">
                    {props.subtext}
                </div>
            }
        </div>
        {
            props.imagePath &&
            <div className="PageHeader-image">
                <img src={props.imagePath} alt=""/>
            </div>
        }
    </header>
);

export default PageHeader;