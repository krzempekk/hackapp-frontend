import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import validation from "react-validation-mixin";
import strategy from "react-validatorjs-strategy";

import PageHeader from '../PageHeader';

import { Auth } from '../../external';

import './Login.scss';

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            redirectToReferrer: Auth.isAuthenticated,
            login: '',
            password: ''
        };

        this.validatorTypes = strategy.createInactiveSchema(
            {
                login: 'required',
                password: 'required',
            }
        );
    }

    getValidatorData = () => {
        const { login, password } = this.state;
        return { login, password };
    };

    activateValidation = (e) => {
        strategy.activateRule(this.validatorTypes, e.target.name);
        this.props.handleValidation(e.target.name)(e);
    };

    handleChange = (event) => {
        event.persist();
        const {name, value} = event.target;
        this.setState({
            [name]: value
        },() => {
            this.props.handleValidation(event.target.name)(event);
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.login();
    }

    login = () => {
        Auth.login(() => {
            this.setState({ redirectToReferrer: true });
        });
    };

    getClassname = (inputName) => {
        const isValid = this.props.getValidationMessages(inputName).length === 0;
        if(this.state[inputName] === "" && isValid)
            return "";
        return  isValid ? "valid" : "invalid";
    }

    render() {
        const { from } = this.props.location.state || { from: { pathname: "/" } };
        const { redirectToReferrer, login, password } = this.state;
        const { getValidationMessages } = this.props;
        const { handleChange, activateValidation, getClassname } = this;

        if (redirectToReferrer) {
            return <Redirect to={from} />;
        }

        return (
            <div className="Login">
                <PageHeader text="Log in to your account"/>
                <form action="#" className="Login-form" noValidate onSubmit={this.handleSubmit}>
                    <div className="Login-input">
                        <div>
                            <input className={getClassname("login")}
                                   type="text" name="login" id="login" value={login}
                                   onChange={handleChange} onBlur={activateValidation} required/>
                            <label htmlFor="login" placeholder="Login" alt="Login"></label>
                        </div>
                        <div className="Login-error">{getValidationMessages("login")}</div>
                    </div>
                    <div className="Login-input">
                        <div>
                            <input className={getClassname("password")}
                                   type="password" name="password" id="password" value={password}
                                   onChange={handleChange} onBlur={activateValidation} required/>
                            <label htmlFor="password" placeholder="Password" alt="Password"></label>
                        </div>
                        <div className="Login-error">{getValidationMessages("password")}</div>
                    </div>
                    <button className="Login-submit">Login</button>
                </form>
            </div>
        );
    }
}

export default validation(strategy)(Login);
