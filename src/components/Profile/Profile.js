import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import PageHeader from '../PageHeader';

import { ProfileInfo } from "../../external";

import './Profile.scss';

import userTeams from '../../icons/userTeams.png';
import joinRequests from '../../icons/joinRequests.png';

class Profile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            surname: "",
            quotation: ""
        }
    }

    async componentDidMount() {
        ProfileInfo.getBasicInfo()
            .then(data => this.setState(data));
    }

    render() {
        const { name, surname, quotation } = this.state;
        return (
            <div className="Profile">
                <PageHeader text={`${name} ${surname}`} subtext={quotation} />
                <div className="Profile-options">
                    <Link className="Profile-option" to="/notifications">
                        <img src={joinRequests} alt=""/>
                        Join requests
                        <div className="Profile-badge"><div>4</div></div>
                    </Link>
                    <Link className="Profile-option" to="/teams">
                        <img src={userTeams} alt=""/>
                        My teams
                        <div className="Profile-badge"><div>7</div></div>
                    </Link>
                </div>
            </div>
        )
    }
}

export default Profile;