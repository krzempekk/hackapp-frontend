import React, { Component } from 'react';

import PageHeader from '../PageHeader';

import './Notifications.scss';

class Notifications extends Component {
    render() {
        return (
            <div>
                <PageHeader text="Notifications"/>
                <ul className="timeline">
                    <li className="event" data-date="10 minutes ago">
                        <h3>lorem ipsum</h3>
                    </li>
                    <li className="event" data-date="5 hours ago">
                        <h3>lorem ipsum</h3>
                    </li>
                    <li className="event" data-date="thursday">
                        <h3>lorem ipsum</h3>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Notifications;