import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import { Auth } from "../../external";

import "./Sidebar.scss";

class Sidebar extends Component {
    render() {
        console.log(Auth);
        return (
            <aside className="Sidebar">
                <div className="Sidebar-container">
                    <div className="Sidebar-header">
                        <Link to="/">HackWorld</Link>
                    </div>
                    <div className="Sidebar-content">
                        {
                            Auth.isAuthenticated
                                ?
                                <div>
                                    <button className="Sidebar-link" onClick={() => {Auth.logout(() => this.props.history.push("/"))}}>Logout</button>
                                    <Link className="Sidebar-link" to="/profile">Profile</Link>
                                </div>
                                :
                                <div>
                                    <Link className="Sidebar-link" to="/login">Login</Link>
                                    <Link className="Sidebar-link" to="/register">Register</Link>
                                </div>
                        }
                    </div>
                </div>
            </aside>
        )
    }
}

export default withRouter(Sidebar);