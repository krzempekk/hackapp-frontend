import React, { Component } from 'react';
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";

import './Hackathon.scss';

import facebookIcon from '../../icons/facebookIcon.png';
import websiteIcon from '../../icons/websiteIcon.png';

class Hackathon extends Component {

    render() {
        const {name, placeName, placeUrl, imageUrl, description, closedTeams, openTeams, officialWebsiteLink, facebookPageLink } = this.props;

        return (
            <SkeletonTheme color="#d1d1d1" highlightColor="#ffffff">
                <article className="Hackathon">
                    {
                        name
                            ?
                            <div className="Hackathon-base">
                                <div>
                                    <div className="Hackathon-name">{name}</div>
                                    {placeUrl
                                        ? <a href={placeUrl} target="_blank" rel="noopener noreferrer">
                                            <div className="Hackathon-place">{placeName}</div>
                                        </a>
                                        : <div className="Hackathon-place">{placeName}</div>
                                    }
                                </div>
                                {imageUrl &&
                                <div className="Hackathon-logo"><img src={imageUrl} alt="Hackathon logo"/></div>}
                            </div>
                            :
                            <Skeleton height={20}/>
                    }
                    <div className="Hackathon-description">{description || <Skeleton count={5} height={20}/>}</div>
                    {closedTeams && openTeams &&
                    <div className="Hackathon-teams">{closedTeams} closed team(s), {openTeams} team(s) looking for
                        members</div>}
                    <div className="Hackathon-links">
                        {officialWebsiteLink &&
                        <a href={officialWebsiteLink} target="_blank" rel="noopener noreferrer">
                            <div className="Hackathon-link"><img src={websiteIcon}
                                                                 alt=""/><span>Hackaton website</span></div>
                        </a>
                        }
                        {facebookPageLink &&
                        <a href={facebookPageLink} target="_blank" rel="noopener noreferrer">
                            <div className="Hackathon-link"><img src={facebookIcon} alt=""/><span>Facebok page</span>
                            </div>
                        </a>
                        }
                    </div>
                </article>
            </SkeletonTheme>
        )
    }
}

export default Hackathon;