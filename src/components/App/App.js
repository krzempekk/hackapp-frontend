import React, { Component } from 'react';
import { BrowserRouter as Router, Switch } from "react-router-dom";

import Sidebar from "../Sidebar"
import MainFeed from "../MainFeed";
import Login from "../Login";
import Profile from "../Profile";
import Notifications from "../Notifications";

import RestrictedRoute from '../../utils';

import "../../styles/allpartials.scss";
import "./App.scss";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Sidebar/>
          <main>
            <Switch>
              <RestrictedRoute exact path="/" component={MainFeed}/>
              <RestrictedRoute exact path="/login" unauthOnly component={Login}/>
              <RestrictedRoute exact path="/profile" authOnly component={Profile}/>
              <RestrictedRoute exact path="/notifications" authOnly component={Notifications}/>
            </Switch>
          </main>
        </div>
      </Router>
    )
  }
}

export default App;
