import React, { Component } from "react";

import PageHeader from "../PageHeader";
import Hackathon from "../Hackathon";

import HackathonsInfo from "../../external";

class MainFeed extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hackathons: []
        }
    }

    async componentDidMount() {
        const data = await HackathonsInfo();
        this.setState({
            hackathons: data
        });
    }

    render() {
        const { hackathons } = this.state;

        return (
            <div className="Feed">
                <PageHeader text="Upcoming hackathons" subtext="Hackathons that will be soon" />
                {
                    hackathons.length
                        ?   hackathons.map((hackathon, index) => <Hackathon key={index} {...hackathon} />)
                        :   <Hackathon/>
                }
            </div>
        )
    }
}

export default MainFeed;