import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { Auth } from '../external';

const RestrictedRoute = ({ component: Component, authOnly = false, unauthOnly = false, ...rest }) => {
    if(!authOnly || Auth.isAuthenticated || (unauthOnly && !Auth.isAuthenticated)) {
        return (
            <Route
                {...rest}
                component={Component}
            />
        );
    }
    else {
        return <Redirect to="/login"/>
    }
}

export default RestrictedRoute;

