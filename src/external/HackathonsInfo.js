const mockHackathons = [
    {
        name: "Hacknarok",
        placeName: "Valhalla",
        description: "Gain everlasting glory!",
        officialWebsiteLink: "http://lalproject.pl",
        closedTeams: 10,
        openTeams: 20
    },
    {
        name: "Bitehack",
        placeName: "Klub Studio",
        description: "Jakiś tam hackatonik",
        officialWebsiteLink: "http://lalproject.pl",
        closedTeams: 15,
        openTeams: 12
    }
]

const HackathonsInfo = async () => {
    return await new Promise((resolve, reject) => {
        setTimeout(() => resolve(mockHackathons), 500);
    })
}

export default HackathonsInfo;
