const mockProfile = {
    name: "Kamil",
    surname: "Krzempek",
    quotation: "SEMJÂZÂ, SÊMIÂZÂZ, SHEMYAZA"
}

export const ProfileInfo = {
    async getBasicInfo() {
        return await new Promise((resolve, reject) => {
            setTimeout(() => resolve(mockProfile), 500);
        });
    }
}