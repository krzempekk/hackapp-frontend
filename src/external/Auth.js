export const Auth = {
    isAuthenticated: !!sessionStorage.getItem("authenticated"),
    login(cb) {
        this.isAuthenticated = true;
        sessionStorage.setItem("authenticated", "true");
        setTimeout(cb, 100);
    },
    logout(cb) {
        this.isAuthenticated = false;
        sessionStorage.removeItem("authenticated");
        setTimeout(cb, 100);
    }
};